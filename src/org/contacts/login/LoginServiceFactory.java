package org.contacts.login;

import java.util.List;
import org.contacts.model.User;

public class LoginServiceFactory {

	private static LoginServiceFactory factory = new LoginServiceFactory();
	private LoginService ls;

	public LoginService getLoginService(List<User> users) {
		if (ls == null) {
			ls = new LoginService(users);
		}
		return ls;
	}

	public static LoginServiceFactory getInstance() {
		return factory;
	}
}
