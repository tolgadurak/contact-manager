package org.contacts.login;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.contacts.model.User;

public class LoginService {
	private Map<String, User> users;

	public LoginService(List<User> users) {
		this.users = new HashMap<String, User>();
		populateUsers(users);
	}

	public void populateUsers(List<User> users) {
		for (User user : users) {
			this.users.put(user.getUserName(), user);
		}
	}

	public User authenticate(User loggingUser) {
		String userName = loggingUser.getUserName();
		if (this.users.containsKey(userName)) {
			User user = users.get(userName);
			if (user.getPassword().equals(loggingUser.getPassword()))
				return user;
		}
		return null;
	}
}
