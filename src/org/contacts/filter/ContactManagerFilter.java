package org.contacts.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.contacts.model.User;

/**
 * Servlet Filter implementation class ContactManagerFilter
 */
@WebFilter("/*")
public class ContactManagerFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public ContactManagerFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpSession session = null;
		HttpServletRequest req = null;
		HttpServletResponse resp = null;
		User user = null;
		String requestedURI = null;
		String queryString = null;
		String attempt = null;
		req = (HttpServletRequest) request;
		resp = (HttpServletResponse) response;
		requestedURI = req.getRequestURI();
		queryString = req.getQueryString();
		session = req.getSession();
		if (session != null) {
			user = (User) session.getAttribute("user");
			if (user == null) {
				if (requestedURI.contains("/UserServlet")
						|| requestedURI.contains("/ContactServlet")) {
					if (queryString != null)
						attempt = requestedURI + "?" + queryString;
					else
						attempt = requestedURI;
					session.setAttribute("attempt", attempt);
					resp.sendRedirect("login.jsp");
				} else {
					chain.doFilter(request, response);
				}
			} else {
				if (requestedURI.contains("/login.jsp")) {
					resp.sendRedirect("index.jsp");
				} else {
					chain.doFilter(request, response);
				}
			}
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
