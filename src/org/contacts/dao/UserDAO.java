package org.contacts.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;

import org.contacts.model.User;

public class UserDAO {
	private EntityManager em;

	public UserDAO(EntityManager em) {
		this.em = em;
	}

	public User findById(Long userId) {
		User user = em.find(User.class, userId);
		if (userId == null) {
			throw new EntityNotFoundException("Can't find user for ID "
					+ userId);
		}
		return user;
	}

	public void createUser(User user) {
		em.getTransaction().begin();
		em.merge(user);
		em.getTransaction().commit();
	}

	public List<User> getUsers() {
		TypedQuery<User> query = em
				.createNamedQuery("findAllUsers", User.class);
		return query.getResultList();
	}

	public void updateUsers(User user) {
		em.getTransaction().begin();
		em.merge(user);
		em.getTransaction().commit();
	}

	public void deleteUserById(Long userId) {
		User user = em.find(User.class, userId);
		if (user != null) {
			em.getTransaction().begin();
			em.remove(user);
			em.getTransaction().commit();
		}
	}

	public void deleteUser(User user) {
		em.getTransaction().begin();
		em.remove(user);
		em.getTransaction().commit();
	}
}
