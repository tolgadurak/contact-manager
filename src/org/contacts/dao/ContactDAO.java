package org.contacts.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;

import org.contacts.model.Contact;
import org.contacts.model.User;

public class ContactDAO {
	private EntityManager em;

	public ContactDAO(EntityManager em) {
		this.em = em;
	}

	public Contact findById(Long contactId) {
		Contact contact = em.find(Contact.class, contactId);
		if (contactId == null) {
			throw new EntityNotFoundException("Can't find Contact for ID "
					+ contactId);
		}
		return contact;
	}

	public List<User> getUsers() {
		TypedQuery<User> query = em
				.createNamedQuery("findAllUsers", User.class);
		return query.getResultList();
	}

	public User findUserById(Long userId) {
		User user = em.find(User.class, userId);
		if (userId == null) {
			throw new EntityNotFoundException("Can't find User for ID "
					+ userId);
		}
		return user;
	}

	public void createContact(Contact contact) {
		em.getTransaction().begin();
		em.merge(contact);
		em.getTransaction().commit();
	}

	public List<Contact> getContacts() {
		TypedQuery<Contact> query = em.createNamedQuery("findAllContacts",
				Contact.class);
		return query.getResultList();
	}

	public void updateContact(Contact contact) {
		em.getTransaction().begin();
		em.merge(contact);
		em.getTransaction().commit();
	}

	public void deleteContactById(Long contactId) {
		Contact contact = em.find(Contact.class, contactId);
		if (contact != null) {
			em.getTransaction().begin();
			em.remove(contact);
			em.getTransaction().commit();
		}
	}
}
