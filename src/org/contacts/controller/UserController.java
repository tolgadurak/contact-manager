package org.contacts.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.contacts.dao.UserDAO;
import org.contacts.model.User;

public class UserController {
	private UserDAO userDao;
	private List<User> users;
	private String jsp;

	public UserController(UserDAO userDao) {
		this.userDao = userDao;
	}

	public void listUsers(HttpServletRequest request) {
		users = userDao.getUsers();
		request.setAttribute("users", users);
		request.setAttribute("page_text", "List Users");
		jsp = "/User/list.jsp";
	}

	public void createUser(HttpServletRequest request) {
		User u = new User();
		request.setAttribute("user", u);
		request.setAttribute("page_text", "Create User");
		jsp = "/User/create.jsp";
	}

	public void editUser(HttpServletRequest request) {
		String idStr = request.getParameter("id");
		long userId = 0;
		if (idStr != null) {
			try {
				userId = Long.parseLong(idStr);
			} catch (NumberFormatException e) {
				userId = 0;
			}
		}
		User u = userDao.findById(userId);
		request.setAttribute("user", u);
		request.setAttribute("page_text", "Edit " + u.getUserName());
		jsp = "/user/edit.jsp";
	}

	public void detailsUser(HttpServletRequest request) {
		String idStr = request.getParameter("id");
		long userId = 0;
		if (idStr != null) {
			try {
				userId = Long.parseLong(idStr);
			} catch (NumberFormatException e) {
				userId = 0;
			}
		}
		User user = userDao.findById(userId);
		request.setAttribute("user", user);
		request.setAttribute("page_text", user.getUserName() + "'s Details");
		jsp = "/user/details.jsp";
	}

	public void deleteUser(HttpServletRequest request) {
		String idStr = request.getParameter("id");
		long userId = 0;
		if (idStr != null) {
			try {
				userId = Long.parseLong(idStr);
			} catch (NumberFormatException e) {
				userId = 0;
			}
			User u = userDao.findById(userId);
			request.setAttribute("user", u);
			jsp = "/user/delete.jsp";

		}
	}

	public void saveUser(HttpServletRequest request, User u) {
		String idStr = request.getParameter("id");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		Long userId = new Long(0);

		if (idStr != null) {
			try {
				userId = Long.parseLong(idStr);

			} catch (NumberFormatException e) {
				userId = new Long(0);
			}
		}
		u.setId(userId);
		u.setUserName(username);
		u.setPassword(password);
		if (u.getId() > 0) {
			userDao.updateUsers(u);
		} else {
			userDao.createUser(u);
		}

	}

	public void deleteUser(HttpServletRequest request, User u) {
		String idStr = request.getParameter("id");
		long userId = 0;
		if (idStr != null) {
			try {
				userId = Long.parseLong(idStr);
				u.setId(userId);
			} catch (NumberFormatException e) {
				userId = 0;
			}
		}
		userDao.deleteUserById(userId);
	}

	public String performDecision(String action, HttpServletRequest request) {
		if (request.getMethod().equals("GET")) {
			if (action.equals("list")) {
				this.listUsers(request);
			} else if (action.equals("create")) {
				this.createUser(request);
			} else if (action.equals("edit")) {
				this.editUser(request);
			} else if (action.equals("details")) {
				this.detailsUser(request);
			} else if (action.equals("delete")) {
				this.deleteUser(request);
			}

		} else if (request.getMethod().equals("POST")) {
			User u = new User();
			if (action.equals("save_edit")) {
				this.saveUser(request, u);
			} else if (action.equals("delete_item")) {
				this.deleteUser(request, u);
			}
		}
		return jsp;
	}
}
