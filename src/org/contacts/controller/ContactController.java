package org.contacts.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.contacts.dao.ContactDAO;
import org.contacts.model.Contact;
import org.contacts.model.User;

public class ContactController {
	private ContactDAO contactDao;
	private List<Contact> contacts;
	private String jsp;

	public ContactController(ContactDAO contactDao) {
		this.contactDao = contactDao;
	}

	public void listContacts(HttpServletRequest request) {
		contacts = contactDao.getContacts();
		request.setAttribute("contacts", contacts);
		request.setAttribute("page_text", "List Contacts");
		jsp = "/contact/list.jsp";
	}

	public void listMyContacts(HttpServletRequest request) {
		String strId = request.getParameter("id");
		Long userId = null;
		if (strId != null) {
			try {
				userId = Long.parseLong(strId);

			} catch (NumberFormatException e) {
				userId = new Long(0);
			}
		}
		User user = contactDao.findUserById(userId);
		contacts = user.getContacts();
		user.setUserName(contacts.get(0).getUser().getUserName());
		request.setAttribute("mycontacts", contacts);
		request.setAttribute("username", user.getUserName());
		jsp = "/contact.jsp";
	}

	public void createContact(HttpServletRequest request) {
		Contact c = new Contact();
		List<User> users = contactDao.getUsers();
		request.setAttribute("contact", c);
		request.setAttribute("users", users);
		jsp = "/contact/create.jsp";
	}

	public void saveContact(HttpServletRequest request, Contact c) {
		String idStr = request.getParameter("id");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String phone = request.getParameter("phone");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String user = request.getParameter("user");
		String active = request.getParameter("isactive");
		Long contactId = new Long(0);
		if (idStr != null) {
			try {
				contactId = Long.parseLong(idStr);
			} catch (NumberFormatException e) {
				contactId = new Long(0);
			}
		}
		c.setId(contactId);
		c.setFirstName(firstname);
		c.setLastName(lastname);
		c.setPhone(phone);
		c.setEmail(email);
		c.setAddress(address);
		c.setActive(Boolean.parseBoolean(active));
		User u = contactDao.findUserById(Long.parseLong(user));
		c.setUser(u);

		c.setCreatedDate(new Date());
		c.setLastModifiedDate(new Date());

		if (c.getId() > 0) {
			contactDao.updateContact(c);
		} else {
			contactDao.createContact(c);
		}
	}

	public String performDecision(String action, HttpServletRequest request) {
		if (request.getMethod().equals("GET")) {
			if (action.equals("list")) {
				this.listContacts(request);
			} else if (action.equals("create")) {
				this.createContact(request);
			} else if (action.equals("edit")) {

			} else if (action.equals("delete")) {

			} else if (action.equals("mycontacts")) {
				this.listMyContacts(request);
			}
		} else if (request.getMethod().equals("POST")) {
			Contact c = new Contact();
			if (action.equals("save_edit")) {
				this.saveContact(request, c);
			} else if (action.equals("delete_item")) {

			}
		}
		return jsp;
	}
}
