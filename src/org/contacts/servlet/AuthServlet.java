package org.contacts.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.contacts.dao.UserDAO;
import org.contacts.login.LoginService;
import org.contacts.login.LoginServiceFactory;
import org.contacts.model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/AuthServlet")
public class AuthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userDao;
	private EntityManagerFactory emf;
	private EntityManager em;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AuthServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		emf = (EntityManagerFactory) config.getServletContext().getAttribute(
				"emf");
		em = emf.createEntityManager();
		userDao = new UserDAO(em);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = null;
		Cookie cookie = null;
		String action = request.getParameter("action");
		if (action != null) {
			if (action.equals("login")) {
				String username = request.getParameter("user_name");
				String password = request.getParameter("user_password");
				User user = new User();
				user.setUserName(username);
				user.setPassword(password);
				LoginServiceFactory loginServiceFactory = LoginServiceFactory
						.getInstance();
				LoginService loginService = loginServiceFactory
						.getLoginService(userDao.getUsers());
				User outcome = loginService.authenticate(user);
				if (outcome != null) {
					session = request.getSession(true);
					outcome.setPassword(null);
					session.setAttribute("user", outcome);
					cookie = new Cookie("sessionId", session.getId());
					response.addCookie(cookie);
					if (session.getAttribute("attempt") != null)
						response.sendRedirect(session.getAttribute("attempt")
								.toString());
					else
						response.sendRedirect("index.jsp");
				} else {
					response.getWriter()
							.write("Username or password is not correct. Please <a href=\"login.jsp\">try again</a>");
				}
			} else if (action.equals("logout")) {
				session = request.getSession();
				session.removeAttribute("username");
				session.invalidate();
				response.sendRedirect("index.jsp");
			}
		}
	}

}
