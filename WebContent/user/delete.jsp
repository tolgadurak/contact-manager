<%@page import="org.contacts.model.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	User u = (User) request.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete</title>
</head>
<body>
	<h1>Delete</h1>
	<h2>Are you sure you want to delete this?</h2>
	<form action="UserServlet" method="post">
		<input type="hidden" name="id" value="<%=u.getId()%>" /> <input
			type="hidden" name="action" value="delete_item" />
		<table>
			<tr>
				<td>User name:</td>
				<td><%=u.getUserName()%></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><%=u.getPassword()%></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>
		<a href="UserServlet?action=list">Back to user list</a> <input
			type="submit" value="Delete" />
	</form>
	<a href="UserServlet?action=list">Back to user list</a>
	<div><a href="index.jsp">Go to index page</a></div>
</body>
</html>