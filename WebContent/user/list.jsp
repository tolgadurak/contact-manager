<%@page import="java.util.ArrayList"%>
<%@page import="org.contacts.model.User"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%
	@SuppressWarnings("unchecked")
	List<User> users = (ArrayList<User>) request.getAttribute("users");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1><%=request.getAttribute("page_text")%></h1>

	<table border="1" cellpadding="1" cellspacing="1">
		<tr>
			<th>User name</th>
			<th>Password</th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
		<%
			for (User u : users) {
		%>
		<tr>
			<td><%=u.getUserName()%></td>
			<td><%=u.getPassword()%></td>
			<td><a href="UserServlet?action=edit&id=<%=u.getId()%>">Edit</a></td>
			<td><a href="UserServlet?action=delete&id=<%=u.getId()%>">Delete</a></td>
			<td><a href="UserServlet?action=details&id=<%=u.getId()%>">Details</a></td>
		</tr>
		<%
			}
		%>

	</table>

	<a href="UserServlet?action=create">Create new user</a>
	<div>
		<a href="index.jsp">Go to index page</a>
	</div>
</body>
</html>