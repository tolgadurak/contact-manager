<%@page import="org.contacts.model.Contact"%>
<%@page import="org.contacts.model.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	User u = (User) request.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1><%=request.getAttribute("page_text")%></h1>
	<h2>Contacts</h2>
	<table border="1" cellpadding="1" cellspacing="1">
		<tr>
			<th>Full Name</th>
			<th>Phone</th>
			<th>Address</th>
			<th>E-mail</th>
			<th>Created Date</th>
			<th>Updated Date</th>
			<th>Active</th>
		</tr>

		<%
			for (Contact c : u.getContacts()) {
		%>
		<tr>
			<td><%=c.getFirstName() + " " + c.getLastName()%></td>
			<td><%=c.getPhone()%></td>
			<td><%=c.getAddress()%></td>
			<td><%=c.getEmail()%></td>
			<td><%=c.getCreatedDate()%></td>
			<td><%=c.getLastModifiedDate()%></td>
			<td><%=c.isActive()%></td>
		</tr>
		<%
			}
		%>


	</table>
</body>
</html>