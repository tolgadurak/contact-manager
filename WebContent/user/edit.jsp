<%@page import="org.contacts.model.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	User u = (User) request.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1><%=request.getAttribute("page_text")%></h1>
	<form action="UserServlet" method="post">
		<table>
			<tr>
				<td>User name:</td>
				<td><input type="text" name="username"
					value="<%=u.getUserName()%>" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="password"
					value="<%=u.getPassword()%>" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Save User" /></td>
			</tr>
		</table>
		<input type="hidden" name="id" value="<%=u.getId()%>" /> <input
			type="hidden" name="action" value="save_edit" />
	</form>
	<a href="UserServlet?action=list">Back to user list</a>
	<div><a href="index.jsp">Go to index page</a></div>
</body>
</html>