<%@page import="org.contacts.model.Contact"%>
<%@page import="java.util.List"%>
<%@page import="org.contacts.model.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%
	@SuppressWarnings("unchecked")
	List<User> users = (List<User>) request.getAttribute("users");
	Contact c = (Contact) request.getAttribute("contact");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create Contact</title>
</head>
<body>
	<h1>Create Contact</h1>
	<form action="ContactServlet" method="post">
		<table>
			<tr>
				<td>First name:</td>
				<td><input type="text" name="firstname" /></td>
			</tr>
			<tr>
				<td>Last name:</td>
				<td><input type="text" name="lastname" /></td>
			</tr>
			<tr>
				<td>Phone:</td>
				<td><input type="text" name="phone" /></td>
			</tr>
			<tr>
				<td>Address</td>
				<td><textarea name="address"></textarea></td>
			</tr>
			<tr>
				<td>E-mail</td>
				<td><input type="text" name="email" /></td>
			</tr>
			<tr>
				<td>Active</td>
				<td><input type="checkbox" name="isactive" /></td>
			</tr>
			<tr>
				<td>User:</td>
				<td><select name="user">
						<%
							for (User u : users) {
						%>
						<option value="<%=u.getId()%>"><%=u.getUserName()%></option>
						<%
							}
						%>
				</select></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Save Contact" /></td>
			</tr>

		</table>
		<input type="hidden" name="id" value="<%=c.getId()%>" /> <input
			type="hidden" name="action" value="save_edit" />
	</form>

	<div>
		<a href="ContactServlet?action=list">Back to contact list</a>
	</div>
	<div>
		<a href="index.jsp">Go to index page</a>
	</div>
</body>
</html>