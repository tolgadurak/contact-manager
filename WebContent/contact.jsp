<%@page import="java.util.List"%>
<%@page import="org.contacts.model.Contact"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%
	String username = (String) request.getAttribute("username");
	@SuppressWarnings("unchecked")
	List<Contact> contacts = (List<Contact>) request
			.getAttribute("mycontacts");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>My contacts</title>
</head>
<body>
<h1>My Contacts</h1>
<table cellpadding="1" cellspacing="1" border="1">
		<tr>
			<th>Full Name</th>
			<th>Phone</th>
			<th>Address</th>
			<th>E-mail</th>
			<th>User name</th>
			<th>Created Date</th>
			<th>Updated Date</th>
			<th>Active</th>
		</tr>
		<%
			for (Contact c : contacts) {
		%>
		<tr>
			<td><%=c.getFirstName() + " " + c.getLastName()%></td>
			<td><%=c.getPhone()%></td>
			<td><%=c.getAddress()%></td>
			<td><%=c.getEmail()%></td>
			<td><a href="UserServlet?action=details&id=<%=c.getUser().getId()%>"><%=c.getUser().getUserName()%></a></td>
			<td><%=c.getCreatedDate()%></td>
			<td><%=c.getLastModifiedDate()%></td>
			<td><%=c.isActive()%></td>
		</tr>
		<%
			}
		%>

	</table>
</body>
</html>