<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login Page</title>
</head>
<body>
<form action="AuthServlet" method="post">
		<table>
			<tr>
				<td>User Name:</td>
				<td><input type="text" name="user_name" /></td>
			</tr>
			<tr>
				<td>User Password:</td>
				<td><input type="password" name="user_password" /></td>
			</tr>
			<tr>
				<td><input type="hidden" name="action" value="login"/></td>
				<td><input type="submit" value="Login"  /></td>
			</tr>
		</table>
	</form>
<div><a href="index.jsp">Go to index page</a></div>

</body>
</html>