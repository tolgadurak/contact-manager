<%@page import="org.contacts.model.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	User user = (User) session.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Index Page</title>
</head>
<body>
	<div>
		<%
			if (user != null) {
		%>
		<span><%=user.getUserName() + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<ul>
				<li><input type="hidden" name="action" value="logout" /> <input
					type="submit" value="Logout" /></li>
				<li><a href="ContactServlet?action=mycontacts&id=<%=user.getId()%>">Show
						My Contacts</a></li>
			</ul>
		</form>
		<%
			} else {
		%>
		You are not logged in, <a href="login.jsp"> Go to login page</a>
		<%
			}
		%>

	</div>
	<div>
		<ul>
			<li><a href="UserServlet?action=list">List All Users</a></li>
			<li><a href="ContactServlet?action=list">List All Contacts</a></li>
			<li><a href="UserServlet?action=create">Create New User</a></li>
			<li><a href="ContactServlet?action=create">Create New
					Contacts</a></li>
		</ul>
	</div>
</body>
</html>